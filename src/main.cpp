#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <Arduino.h>
#include "Thing.h"
#include "WebThingAdapter.h"

#ifndef STASSID
#define STASSID "Parastoo"
#define STAPSK "P@r@s200P!!8"
#endif

const char *ssid = STASSID;
const char *password = STAPSK;

const int ledPin = LED_BUILTIN;
const int doorPin = D1;
const int bellPin = D2;

unsigned long previousMillis = 0;

WebThingAdapter *adapter;

const char *types[] = {"OnOffSwitch", nullptr};
ThingDevice frontDoor("frontDoor", "Front Door", types);
ThingProperty doorOpen("on", "", BOOLEAN, "OnOffProperty");

void turnOffSwitch()
{
  ThingPropertyValue newProperty = {false};
  newProperty.boolean = false;
  doorOpen.setValue(newProperty);
}

void setup(void)
{
  pinMode(ledPin, OUTPUT);
  pinMode(bellPin, OUTPUT);
  pinMode(doorPin, OUTPUT);

  digitalWrite(ledPin, HIGH);

  Serial.begin(115200);
  Serial.println("");
  Serial.print("Connecting to \"");
  Serial.print(ssid);
  Serial.println("\"");

#if defined(ESP8266) || defined(ESP32)
  WiFi.mode(WIFI_STA);
#endif

  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  bool blink = true;
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    digitalWrite(ledPin, blink ? LOW : HIGH); // active low led
    blink = !blink;
  }
  digitalWrite(ledPin, HIGH); // active low led

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  adapter = new WebThingAdapter("w25", WiFi.localIP());

  frontDoor.addProperty(&doorOpen);
  adapter->addDevice(&frontDoor);
  adapter->begin();
  Serial.println("HTTP server started");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print("/things/");
  Serial.println(frontDoor.id);

  turnOffSwitch();
}

void loop(void)
{
  unsigned long currentMillis = millis();

  if (previousMillis > 0)
  {

    if (currentMillis - previousMillis >= 2500)
    {
      digitalWrite(doorPin, LOW);

      turnOffSwitch();
      previousMillis = 0;

      Serial.print("Door Opener Off");
      Serial.println(currentMillis);
    }
    else if (currentMillis - previousMillis >= 2000)
    {
      digitalWrite(bellPin, LOW);
      digitalWrite(doorPin, HIGH);

      Serial.print("Bell Off");
      Serial.print("Door Opener On");
      Serial.println(currentMillis);
    }

    return;
  }
  adapter->update();
  bool on = doorOpen.getValue().boolean;

  digitalWrite(bellPin, on ? HIGH : LOW);

  if (on)
  {
    previousMillis = currentMillis;

    Serial.print("Bell On");
    Serial.println(currentMillis);
  }
}